import unittest

import pytest
from src.utils import Analyzer
from src.training.train_pipeline import TrainingPipeline
from src.db.models import Prediction

class TestUnitApi(unittest.TestCase):
    def test_analyze_data_function(self):
        """
            Check the imported data from the csv file located in DATASET_
        """
        a = Analyzer()
        self.assertIsNotNone(a.analyze_data())

    def test_training_pipline(self):
        """
            Check the training class functions
        """
        tp = TrainingPipeline()
        tp.train()
        self.assertIsNotNone(tp.get_model_perfomance())

    def test_prediction_database_model(self):
        """
            Checking the Prediction model which is saved to the database
        """
        prediction = Prediction(
            feature4=1.1247,
            feature11=-4.5452,
            feature12=-0.2471,
            feature14=-9.1412,
            result="Clear"
        )

        self.assertEqual(prediction.feature4, 1.1247)
        self.assertEqual(prediction.feature11, -4.5452)
        self.assertEqual(prediction.feature12, -0.2471)
        self.assertEqual(prediction.feature14, -9.1412)
        self.assertEqual(prediction.result, "Clear")

