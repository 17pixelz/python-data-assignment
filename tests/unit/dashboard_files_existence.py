import os
import unittest

from dashboard.constants import *


class TestFiles(unittest.TestCase):
    def test_dashboards_files_existence(self):
        """
            Check if all files mentioned in dashboard/constants.py file exist
        """
        files_paths = [CM_PLOT_PATH]
        for file_path in files_paths:
            self.assertTrue(os.path.isfile(file_path))
