import os
import unittest

from src.constants import *


class TestFiles(unittest.TestCase):
    def test_api_files_existence(self):
        """
            Check if all files mentioned in constants.py file exist
        """
        files_paths = [DATASET_PATH, DECISION_TREE_MODEL_PATH, SVC_MODEL_PATH, NAIVE_BAYES_MODEL_PATH,
                       AGGREGATOR_MODEL_PATH]
        for file_path in files_paths:
            self.assertTrue(os.path.isfile(file_path))
