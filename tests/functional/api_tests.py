import unittest
import requests

from src.api import app
from src.db.models import Prediction


class TestFunctionalApi(unittest.TestCase):
    BASE_URL = "http://localhost:5000/"
    TRAINING_URL = BASE_URL + "api/training/"
    INFERENCE_URL = BASE_URL + "api/inference/"
    ANALYSIS_URL = BASE_URL + "api/analysis/"

    def test_requesting_get_routes(self):
        """
            Check each and every get route on the application
        """
        with app.app_context():
            response = requests.get(self.BASE_URL)
            self.assertEqual(response.status_code, 200)
            response = requests.get(self.TRAINING_URL)
            self.assertEqual(response.status_code, 200)
            response = requests.get(self.INFERENCE_URL)
            self.assertEqual(response.status_code, 200)
            response = requests.get(self.ANALYSIS_URL)
            self.assertEqual(response.status_code, 200)

    def test_post_data_to_analysis_route(self):
        """
            my name is explaining everything (even am the most vague name in the whole test functions)
        """
        with app.app_context():
            try:
                result = requests.post(
                    self.ANALYSIS_URL,
                )
            except Exception as e:
                return e
            self.assertEqual(result.status_code, 200)

    def test_post_data_to_training_route(self):
        """
            my name is explaining everything (even am the most vague name in the whole test functions)
        """
        with app.app_context():
            data = {
                "serialize": False,
                "name": "test"
            }
            try:
                result = requests.post(
                    self.TRAINING_URL,
                    json=data
                )
            except Exception as e:
                print(f"ooh sad exception : {e}")
            self.assertEqual(result.status_code, 200)

    def test_post_data_to_inference_route(self):
        """
            my name is explaining everything too
        """
        with app.app_context():
            data = [5.1, 1.5, 1.1, 5.5]
            try:
                result = requests.post(
                    self.INFERENCE_URL,
                    json=data
                )
            except Exception as e:
                print(f"ooh sad exception : {e}")
            return result.json()

    def test_get_all_last_10_records_from_inference_route(self):
        """
            Same here
        """
        with app.app_context():
            try:
                result = requests.get(
                    self.INFERENCE_URL+"all",
                )
            except Exception as e:
                print(f"ooh sad exception : {e}")
            self.assertEqual(result.status_code, 200)

    def test_get_all_last_10_records_from_database(self):
        """
            Checking if the queried dara from database is not none
        """
        with app.app_context():
            self.assertIsNotNone(Prediction.get_all())
