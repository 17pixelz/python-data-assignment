import streamlit as st
from dashboard.pages import eda, training, inference
from dashboard.multi_pages import MultiPages


app = MultiPages()

#################################
#   Page layout configuration   #
#################################
st.set_page_config(page_title="Card Fraud Detection Dashboard - Assignment", layout='wide')


#################################
#   Page Title and sub title    #
#################################
st.markdown("<h1 style='text-align:center'>Card Fraud Detection Dashboard</h1>", unsafe_allow_html=True)
st.markdown("<h5 style='text-align:center'>Made By <a href='https://gitlab.com/elmesaoudee' target='_blank'>EL Mesaoudi Zakariae</a> \n"
            "updated By <a href='https://gitlab.com/17pixelz' target='_blank'>17pixelz</a></h5>",
            unsafe_allow_html=True)



# Add all your application here
app.add_app("EAD", eda.app)
app.add_app("Training", training.app)
app.add_app("Inference", inference.app)
# The main app
app.run()