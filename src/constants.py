from pathlib import Path

PARENT_PATH = Path(__file__).parent.parent

INPUT_PATH = PARENT_PATH / "input/"
DATASET_PATH = INPUT_PATH / "resampled_data.csv"

OUTPUT_PATH = PARENT_PATH / "output/"

PLOTS_PATH = OUTPUT_PATH / "plots/"
EDA_PLOTS_PATH = PLOTS_PATH / "eda/"

MODELS_PATH = OUTPUT_PATH / "models"

DECISION_TREE_MODEL_PATH = MODELS_PATH / "decisiontree.joblib"
SVC_MODEL_PATH = MODELS_PATH / "svc.joblib"
NAIVE_BAYES_MODEL_PATH = MODELS_PATH / "naive_bayes_model.joblib"
AGGREGATOR_MODEL_PATH = MODELS_PATH / "mix.joblib"
DEFAULT_MODEL_PATH = MODELS_PATH / "model.joblib"

CM_PLOT_PATH = PLOTS_PATH / "cm_plot.png"

DATABASE_URI = "sqlite:///../inferences.db"
