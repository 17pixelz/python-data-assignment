from dataclasses import dataclass
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

@dataclass
class Prediction(db.Model):
    id: int
    feature4: float
    feature11: float
    feature12: float
    feature14: float
    result: str

    __tablename__ = "PREDICTIONS"

    id = db.Column(db.Integer, primary_key=True, index=True)
    feature4 = db.Column(db.Float)
    feature11 = db.Column(db.Float)
    feature12 = db.Column(db.Float)
    feature14 = db.Column(db.Float)
    result = db.Column(db.String)

    def insert(self):
        db.session.add(self)
        db.session.commit()

    @staticmethod
    def get_all():
        all_predictions = Prediction.query.order_by(Prediction.id).limit(10).all()
        return all_predictions
