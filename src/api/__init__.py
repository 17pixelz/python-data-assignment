from flask import Flask
from src.db.models import db
from src.constants import DATABASE_URI

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = DATABASE_URI
db.init_app(app)

from src.api.index_routes import blueprint as index_blueprint
from src.api.inference_routes import blueprint as inference_blueprint
from src.api.analysis_routes import blueprint as analysis_blueprint
from src.api.training_routes import blueprint as training_blueprint

app.register_blueprint(index_blueprint)
app.register_blueprint(inference_blueprint)
app.register_blueprint(analysis_blueprint)
app.register_blueprint(training_blueprint)

if __name__ == "__main__":
    with app.app_context():
        db.create_all()
        db.session.commit()
    app.run()
