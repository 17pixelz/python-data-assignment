import numpy as np

from flask import Blueprint, request, jsonify
from src.constants import AGGREGATOR_MODEL_PATH
from src.models.aggregator_model import AggregatorModel
from src.db.models import Prediction

model = AggregatorModel()
model.load(AGGREGATOR_MODEL_PATH)

blueprint = Blueprint('inference', __name__, url_prefix='/api/inference')


@blueprint.route('/')
@blueprint.route('/index')
def index():
    return "CARD FRAUD DETECTION API - INFERENCE BLUEPRINT"


@blueprint.route('/all')
def get_all_inferences():
    return jsonify(Prediction.get_all())


@blueprint.route('/', methods=['POST'])
def run_inference():
    data = request.json
    features = np.array(data).reshape(1, -1)
    prediction_result = model.predict(features)
    prediction = Prediction(
        feature4=data[0],
        feature11=data[1],
        feature12=data[2],
        feature14=data[3],
        result="Clear" if str(prediction_result[0]) == '0' else "Fraudulant"
    )
    prediction.insert()

    return str(prediction_result[0])
