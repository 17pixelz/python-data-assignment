from flask import Blueprint, request
from src.training.train_pipeline import TrainingPipeline

blueprint = Blueprint('training', __name__, url_prefix='/api/training')


@blueprint.route('/')
@blueprint.route('/index')
def index():
    return "CARD FRAUD DETECTION API - TRAINING BLUEPRINT"


@blueprint.route('/', methods=['POST'])
def train():
    info = request.json
    tp = TrainingPipeline()
    tp.train(serialize=info["serialize"],
             model_name=info["name"]
             )
    tp.render_confusion_matrix(plot_name=info["name"])
    accuracy, f1 = tp.get_model_perfomance()
    return {
        "accuracy": accuracy,
        "f1": f1,
    }
