from flask import Blueprint
from src.utils import Analyzer


blueprint = Blueprint('analysis', __name__, url_prefix='/api/analysis')


@blueprint.route('/')
@blueprint.route('/index')
def index():
    return "CARD FRAUD DETECTION API - ANALYSIS BLUEPRINT"


@blueprint.route('/', methods=['POST'])
def analyze():
    try:
        analyzer = Analyzer()
        analyzer.analyze_data()
    except Exception as e:
        return e
    return "Done"