from sklearn.svm import SVC

from src.models.base_model import BaseModel


class SVCModel(BaseModel):
    def __init__(self, C: int = 0.1, kernel: str = 'linear'):
        self.C = C
        self.kernel = kernel
        super().__init__(
            model=SVC(
                C=self.C,
                kernel=self.kernel
            )
        )
