import numpy as np
import itertools
import matplotlib.pyplot as plt
import pandas as pd

import seaborn as sea

from src.constants import DATASET_PATH, INPUT_PATH, EDA_PLOTS_PATH


class PlotUtils:
    @staticmethod
    def plot_confusion_matrix(cm, classes, title, normalize=False, cmap=plt.cm.Blues):
        title = 'Confusion Matrix of {}'.format(title)

        if normalize:
            cm = cm.astype(float) / cm.sum(axis=1)[:, np.newaxis]

        plt.imshow(cm, interpolation='nearest', cmap=cmap)
        plt.title(title)
        plt.colorbar()
        tick_marks = np.arange(len(classes))
        plt.xticks(tick_marks, classes, rotation=45)
        plt.yticks(tick_marks, classes)

        fmt = '.2f' if normalize else 'd'
        thresh = cm.max() / 2.
        for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
            plt.text(
                j,
                i,
                format(cm[i, j], fmt),
                horizontalalignment='center',
                color='white' if cm[i, j] > thresh else 'black'
            )

        plt.tight_layout()
        plt.ylabel('True label')
        plt.xlabel('Predicted label')


class Analyzer():
    def analyze_data(self):
        self.data = pd.read_csv(DATASET_PATH)
        # self.get_percentage()
        # self.get_counts()
        # self.get_amount_stats()
        # self.get_features_stats()
        return self.data

    def get_percentage(self):
        fig, ax = plt.subplots(1, 1)
        ax.pie(self.data.Class.value_counts(), autopct='%1.1f%%', labels=['Clear', 'Fraud'],
               colors=['yellowgreen', 'r'])
        plt.axis('equal')
        plt.ylabel('')
        plt.savefig(EDA_PLOTS_PATH / 'percentage_res.png')

    def get_features_stats(self):
        for i, col in enumerate(self.data[self.data.iloc[:, 1:29].columns]):
            _, ax5 = plt.subplots(1, 1)
            sea.distplot(self.data[col][self.data.Class == 1], bins=50, color='r')
            sea.distplot(self.data[col][self.data.Class == 0], bins=50, color='g')
            ax5.set_xlabel('')
            ax5.set_title('feature: ' + str(col))
            file_name = f"features/feature{i}.png"
            plt.savefig(EDA_PLOTS_PATH / file_name)

    def get_counts(self):
        count_classes = pd.value_counts(self.data['Class'], sort=True).sort_index()
        count_classes.plot(kind='bar')
        fig, ax = plt.subplots(1, 1)
        ax.bar(["Clear", "Fraud"], count_classes[:], 0.35, color='b')
        ax.set_ylabel('Count')
        ax.set_title('Count classes histogram')
        plt.savefig(EDA_PLOTS_PATH / "counts_res.png")
        return count_classes[0], count_classes[1]

    def get_amount_stats(self):
        fig, (ax3, ax4) = plt.subplots(2, 1, figsize=(6, 5), sharex=True)
        ax3.hist(self.data.Amount[self.data.Class == 0], bins=50, color='g', alpha=0.5)
        ax3.set_yscale('log')
        ax3.set_title('Clear')
        ax3.set_ylabel('# transactions')
        ax4.hist(self.data.Amount[self.data.Class == 1], bins=50, color='r', alpha=0.5)
        ax4.set_yscale('log')
        ax4.set_title('Fraud')
        ax4.set_xlabel('Amount')
        ax4.set_ylabel('# transactions')
        plt.savefig(EDA_PLOTS_PATH / "amount_stats.png")

    def ressampler(self):
        number_records_fraud = len(self.data[self.data.Class == 1])
        fraud_indices = np.array(self.data[self.data.Class == 1].index)

        normal_indices = self.data[self.data.Class == 0].index

        random_normal_indices = np.random.choice(normal_indices, number_records_fraud, replace=False)
        random_normal_indices = np.array(random_normal_indices)

        under_sample_indices = np.concatenate([fraud_indices, random_normal_indices])

        under_sample_data = self.data.iloc[under_sample_indices, :].iloc[1:]

        under_sample_data.to_csv(INPUT_PATH / "test.csv", index=False)


if __name__ == "__main__":
    a = Analyzer()
    data = a.analyze_data()
    a.ressampler()
