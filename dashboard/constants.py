from pathlib import Path


PARENT_PATH = Path(__file__).parent.parent

INPUT_PATH = PARENT_PATH / "input/"

OUTPUT_PATH = PARENT_PATH / "output/"

PLOTS_PATH = OUTPUT_PATH / "plots/"
EDA_PLOTS_PATH = PLOTS_PATH / "eda/"

CM_PLOT_PATH = PLOTS_PATH / "cm_plot.png"
