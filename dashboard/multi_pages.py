import streamlit as st


class MultiPages:
    def __init__(self):
        self.apps = []


    def add_app(self, title, func):
        self.apps.append({
            "title": title,
            "function": func
        })


    def run(self):
        st.sidebar.title("Navigate through the app")
        app = st.sidebar.selectbox(
            "Naviagation",
            self.apps,
            format_func=lambda app: app['title']
        )

        app['function']()
