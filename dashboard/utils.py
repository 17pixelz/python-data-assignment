import requests
import pandas as pd

def get_prediction(data):
    try:
        result = requests.post(
            'http://localhost:5000/api/inference',
            json=data
        )
        prediction = result.json()
        return prediction
    except Exception as e:
        return e

def get_all_predections():
    try:
        result = requests.get(
            'http://localhost:5000/api/inference/all',
        )
        data = result.json()
        df = pd.json_normalize(data)
        df.drop('id', axis=1, inplace=True)
        df.index += 1
        return df
    except Exception as e:
        return e

def get_training_data(serialize, name):
    data = {
        "serialize": serialize,
        "name": name
            }
    try:
        result = requests.post(
            'http://localhost:5000/api/training',
            json=data
        )
        res_data = result.json()
        return res_data["accuracy"], res_data["f1"]
    except Exception as e:
        return e


def analyze():
    try:
        result = requests.post(
            'http://localhost:5000/api/analysis',
        )
        return result.json()
    except Exception as e:
        return e


def center_align(s, props='text-align: cenetr;'):
    return props
