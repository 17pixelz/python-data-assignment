import time
import streamlit as st

from dashboard.utils import get_prediction, get_all_predections, center_align

def app():
    st.header("Fraud Inference")
    st.info("This section simplifies the inference process. "
            "You can tweak the values of feature 4, 12, 11, 14"
            "and observe how your model reacts to these changes.")

    feature_4 = st.number_input('Transaction Feature 4', value=5.4851, min_value=float(-10.0), max_value=float(10.0),
                                format="%f")
    feature_11 = st.number_input('Transaction Feature 11', value=2.7855, min_value=float(-10.0), max_value=float(10.0),
                                 format="%f")
    feature_12 = st.number_input('Transaction Feature 12', value=-5.9877, min_value=float(-10.0), max_value=float(10.0),
                                 format="%f")
    feature_14 = st.number_input('Transaction Feature 14', value=-6.1102, min_value=float(-10.0), max_value=float(10.0),
                                 format="%f")
    buttons = st.columns([2,1,1,2])
    infer = buttons[1].button('Run Fraud Inference')
    list = buttons[2].button('List last 10 inferences')

    if infer:
        with st.spinner('Running inference...'):
            time.sleep(1)
            data = [feature_4, feature_11, feature_12, feature_14]
            prediction = get_prediction(data)
            if prediction == 0:
                st.success("This transaction is clear")
            else:
                st.error("This transaction is fraudulent")
    if list:
        with st.spinner('Getting data...'):
            time.sleep(1)
            data = get_all_predections()
            records = st.columns([2, 4, 2])
            records[1].markdown("<h4 style='text-align:center'>the last 10 inferences</h4>", unsafe_allow_html=True)
            records[1].dataframe(data.style.applymap(center_align))
