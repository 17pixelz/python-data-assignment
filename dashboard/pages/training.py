import time
import streamlit as st

from PIL import Image
from src.constants import PLOTS_PATH
from dashboard.utils import get_training_data


def app():
    st.header("Model Training")

    name = st.text_input('Model name', placeholder='decisiontree')
    serialize = st.checkbox('Save model')
    train = st.button('Train Model')

    if train:
        if name == '':
            st.error("Please provide a name for your model")
        else:
            with st.spinner('Training model, please wait...'):
                time.sleep(1)
                try:
                    accuracy, f1 = get_training_data(serialize, name)
                    cols = st.columns([2,4,2])
                    cols[2].metric(label="Accuracy score", value=str(round(accuracy, 4)))
                    cols[2].metric(label="F1 score", value=str(round(f1, 4)))

                    filename = name + ".png"
                    cols[1].image(Image.open(PLOTS_PATH/filename), width=500)
                except Exception as e:
                    st.error('Failed to train model!')
                    st.exception(e)