import streamlit as st
from PIL import Image
from dashboard.constants import EDA_PLOTS_PATH


def app():
    # Header section
    st.header("Exploratory Data Analysis")
    st.caption(
        "In this section we will explore the dataset and it's features in order to come out with some rules to follow.")

    # First Paragraph
    st.subheader("1. Fraud/Clear Classes")
    st.markdown("Let's take a look at the percentage of clear and fraud transactions in our data.")
    classes_cols = st.columns(2)
    classes_cols[0].image(Image.open(EDA_PLOTS_PATH / "percentage.png"))
    classes_cols[1].image(Image.open(EDA_PLOTS_PATH / "counts.png"))

    st.markdown(
        "We notice that data provided is unbalanced, the percentage of fraud transaction is so low compared to clear transaction.")
    st.markdown("**we need to solve this problem**, we can do that either by :")
    st.markdown("1- Collecting more data which is not gonna be easy.")
    st.markdown("2- Resampling the dataset: in order to have an 1/2 probability for each fraud and clear transactions")
    st.markdown("we will choose the second option and try to resample our dataset<br/><br/>", unsafe_allow_html=True)

    st.markdown("<h5>Resampling:</h5>", unsafe_allow_html=True)
    st.markdown("As we mentioned before, we will make the probability of each class equals to 1/2=0.5, "
                "our only way is to reduce the records number of clear class to be the same size as fraud class records, "
                "by removing random records from clear class records.")
    classes_res_cols = st.columns(2)
    classes_res_cols[0].image(Image.open(EDA_PLOTS_PATH / "percentage_res.png"),
                              caption="Percentage of each class after resampling")
    classes_res_cols[1].image(Image.open(EDA_PLOTS_PATH / "counts_res.png"),
                              caption="Count of each class after resampling")
    st.success("After this procedure we will save our data to only work with it, "
               "let's start navigating the features and try to find the useful ones of them.")

    # Second Paragraph
    st.subheader("2. Features selection")
    st.markdown("In this paragraph we will analyse some statistics of our features.")
    # Amount
    st.markdown("<h5>2.1. The <u>Amount</u> feature:</h5>", unsafe_allow_html=True)

    st.columns(3)[1].image(Image.open(EDA_PLOTS_PATH / "amount_stats.png"),
                           caption="amount of fraud and clear transaction")
    st.markdown(
        "We can notice that both the fraud and clear transaction have the same interval of amount of transactions."
        " That will help us of course, so we can leave the amount out of consideration, and focus on other features"
        " to train our model.")

    st.warning("Note that we are working with the resampled data, there is no goal from working with original one.")

    st.markdown("<h5>2.2. The other 28 features:</h5>", unsafe_allow_html=True)
    cols = st.columns(4)
    i = 0
    for j in range(28):
        if j in (7, 14, 21):
            i += 1
        img = f"features/feature{j}.png"
        cols[i].image(Image.open(EDA_PLOTS_PATH / img), use_column_width=True)
    st.markdown(
        "For some of the features, both the classes have similar distribution. "
        "So, we don't expect them to contribute towards classifying power of the model. ")
    st.warning("That does not mean they're useless, but in our case they will lead to nothing but overfitting")
